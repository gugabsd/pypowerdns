from pypowerdns import app
from pypowerdns import db

db.create_all()

app.run(host='0.0.0.0', port=8000, debug=True)