# -*- coding: utf-8 -*-

import os
_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False

SECRET_KEY = 'niuddm7nh235hybh786f67vtbcawkiknl/'

PATH_DB = os.path.join("./var", "main.db")
SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % (PATH_DB)