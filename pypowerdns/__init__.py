# -*- coding: utf-8 -*-

from flask import Flask, render_template, session
from flask_sqlalchemy import SQLAlchemy
from locale import setlocale, LC_ALL
from datetime import datetime

app = Flask(__name__)
app.config.from_object('config')

# Configura locale para pt_BR
setlocale(LC_ALL, "pt_BR.UTF-8")

db = SQLAlchemy(app)

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.context_processor
def contexto_para_templates():
    return dict(
        login=session.get('login') if session.get('login') else "",
        data=unicode(datetime.today().strftime(
            "%A, %d de %B de %Y"
        ), 'utf8')
    )

# Faz a importação das views:
import pypowerdns.views.login
import pypowerdns.views.main