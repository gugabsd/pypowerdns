# -*- coding: utf-8 -*-

from flask.ext.wtf import Form, TextField, PasswordField, validators

class FormLogin(Form):
    """ Formulário de login """
    login = TextField('login', [validators.Length(min=3, max=30)])
    senha   = PasswordField('senha', [validators.Length(min=3, max=30)])