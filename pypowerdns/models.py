# -*- coding: utf-8 -*-

from pypowerdns import db

class Usuario(db.Model):
    __tablename__   = 'usuario'
    id          = db.Column(db.Integer, primary_key=True)
    ligado      = db.Column(db.Boolean)
    login       = db.Column(db.String(50), unique=True)
    senha       = db.Column(db.String(200))
    perfil      = db.Column(db.String(80))
    nome        = db.Column(db.String(150))
    email       = db.Column(db.String(120))
    telefone    = db.Column(db.String(15))
    descricao   = db.Column(db.Text)
    grupo       = db.Column(db.Integer)

    def __init__(
        self,
        ligado,
        login,
        senha,
        perfil,
        nome,
        email,
        telefone,
        descricao,
        grupo
    ):
        self.ligado     = ligado
        self.login      = login
        self.senha      = senha
        self.perfil     = perfil
        self.nome       = nome
        self.email      = email
        self.telefone   = telefone
        self.descricao  = descricao
        self.grupo      = grupo

    def __repr__(self):
        return "<User('%s','%s','%s','%s','%s')>" % (
            self.login,
            self.perfil,
            self.nome,
            self.email,
            self.telefone
        )

class Domain(db.Model):
    __tablename__ = "domains"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)
    master = db.Column(db.String(128))
    last_check = db.Column(db.Integer)
    type = db.Column(db.String(6), nullable=False)
    notified_serial = db.Column(db.Integer)
    account = db.Column(db.String(40))

    def __init__(self, name, master, last_check,
                 type, notified_serial, account):
        self.name = name
        self.master = master
        self.last_check = last_check
        self.type = type
        self.notified_serial = notified_serial
        self.account = account

    def __repr__(self):
        return "<Domain('%s')>" % self.name

class Record(db.Model):
    __tablename__ = "records"
    id = db.Column(db.Integer, primary_key=True)
    domain_id = db.Column(db.Integer, db.ForeignKey(Domain.id))
    name = db.Column(db.String(255))
    type = db.Column(db.String(10))
    content = db.Column(db.Text())
    ttl = db.Column(db.Integer)
    prio = db.Column(db.Integer)
    change_date = db.Column(db.Integer)

    def __init__(self, domain_id, name, type, content, ttl,
                 prio, change_date):
        self.domain_id = domain_id
        self.name = name
        self.type = type
        self.content = content
        self.ttl = ttl
        self.prio = prio
        self.change_date = change_date

    def __repr__(self):
        return "<Record('%s')>" % self.name

class Supermaster(db.Model):
    __tablename__ = "supermasters"
    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(25))
    nameserver = db.Column(db.String(255))
    account = db.Column(db.String(40))

    def __init__(self, ip, nameserver, account):
        self.ip = ip
        self.nameserver = nameserver
        self.account = account

    def __repr__(self):
        return "<Supermaster('%s')>" % self.ip