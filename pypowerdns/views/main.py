# -*- coding: utf-8 -*-

from flask import render_template
from flask.ext.login import login_required
from pypowerdns import app

@app.route('/')
@login_required
def index():
    return render_template('principal.html')