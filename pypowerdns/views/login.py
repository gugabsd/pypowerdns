# -*- coding: utf-8 -*-

from flask import request, redirect, render_template, url_for, flash
from flask.ext.login import (LoginManager, login_required,
                            login_user, logout_user, UserMixin)

from pypowerdns import app
from pypowerdns.libs.forms import FormLogin
import pypowerdns.models as modelos

# flask-login - Configurações de Autenticação:
login_manager = LoginManager()
login_manager.setup_app(app)
login_manager.login_view = "login"

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

@login_manager.user_loader
def load_user(id):
    usuario = modelos.Usuario.query.get(int(id))
    return User(usuario.login, usuario.id)

login_manager.setup_app(app)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = FormLogin(request.form)
    if request.method == 'POST':
        if form.validate():
            _login = request.form['login']
            _senha = request.form['senha']

            _em_db = modelos.Usuario.query.filter_by(login = _login).first()
            if (_em_db):
                if ( _login == _em_db.login and _senha == _em_db.senha):
                    login_user(User(_em_db.login, _em_db.id))
                    redirect(request.args.get("next") or url_for("index"))
                else:
                    flash(unicode("Senha Invalida", "utf8"), "erro")
                    return render_template("login.html", form=form)
            else:
                flash(unicode("Usuário não existe", "utf8"), "erro")
                return render_template("login.html", form=form)
        else:
            flash(unicode("Você deve preencher o formulário", "utf8"), "erro")
            return render_template("login.html", form=form)

    else:
        return render_template("login.html", form=form)

@app.route('/logout')
@login_required
def logout():
    """    Define a remoção da sessão do usuário    """
    logout_user()
    return redirect('/')